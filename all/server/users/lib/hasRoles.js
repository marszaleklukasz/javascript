var hasRoles = function(req, roles) {
    var found = false;
    roles.forEach(function(role) {
        if (role == req.session.user.role) {
            found = true;
        }
    });

    return found;
};

module.exports = hasRoles;