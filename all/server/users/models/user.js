var mongoose = require('mongoose');

var userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        set: function(value) {
            return value.trim().toLowerCase();
        }
    },
    password: String,
    role: {
        type: String,
        enum: [
            'user',
            'editor',
            'admin'
        ]
    }
});

module.exports = mongoose.model('User', userSchema);