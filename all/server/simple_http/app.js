var http = require('http');

http.createServer(function (req, res) {
  res.writeHead(200, {'Content-Type': 'text/plain; charset=utf-8'});
  res.end('Witaj, świecie!\n', 'utf8');
}).listen(3000, '127.0.0.1');

console.log('Serwer działa pod adresem http://127.0.0.1:3000');