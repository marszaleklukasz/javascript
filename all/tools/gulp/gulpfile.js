const gulp = require('gulp');
const sass = require('gulp-sass');
const cssnano = require('gulp-cssnano');
const sourcemaps = require('gulp-sourcemaps');
const rename = require('gulp-rename');
const fileInclude = require('gulp-file-include');
const htmlmin = require('gulp-htmlmin');
const javascriptObfuscator = require('gulp-javascript-obfuscator');
const browserSync = require('browser-sync').create();

const srcAllScss = './src/scss/**/*.scss';
const srcScss = './src/scss/*.scss';
const destCss = './public/css';

function style() {
    return gulp.src(srcScss)
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(cssnano())        
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(destCss))
        .pipe(browserSync.stream());
}

const srcAllTemplates = './src/templates/**/*.html';
const srcTemplates = './src/templates/*.html';
const destTemplates = './public';

function html() {
    return gulp.src(srcTemplates)
        .pipe(fileInclude({
            prefix: '@@',
            basepath: '@file'
        }))
        .pipe(htmlmin({ 
            collapseWhitespace: true,
            removeComments: true,
            removeEmptyAttributes: true
        }))
        .pipe(gulp.dest(destTemplates))
        .pipe(browserSync.stream());
}

const srcAllJavascript = './src/js/**/*.js';
const srcJavascript = './src/js/*.js';
const destJavascript = './public/js';

function javascript() {
    return gulp.src(srcJavascript)
        .pipe(sourcemaps.init())
        .pipe(javascriptObfuscator({
            compact: true
        }))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(destJavascript))
        .pipe(browserSync.stream());
};

function watch() {
    browserSync.init({
        server: './public'
    });

    gulp.watch(srcAllScss, style);
    gulp.watch(srcAllTemplates, html);
    gulp.watch(srcAllJavascript, javascript);
}

exports.style = style;
exports.html = html;
exports.javascript = javascript;
exports.watch = watch;
