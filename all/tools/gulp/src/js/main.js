(function () {
	const getLocationHref = function () {
		return window.location.href;
	}

	const init = function () {
		console.log(`Witaj na stronie ${getLocationHref()}`);
	}

	init();
})();