import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    
    {
      path: '/',
      name: 'login',
      component: require('@/components/LoginPage').default
    },
    {
      path: '/hello',
      name: 'hello',
      component: require('@/components/HelloWorld').default
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
