const createAlert = (message, type = 'warning') => {
  return  `
    <div class="alert alert-${type}" role="alert">
      ${message}
    </div>
  `
}

const sleep = (delay) => new Promise((resolve) => setTimeout(resolve, delay))

export { createAlert, sleep }