import {
  SET_USERNAME,
  SET_PASSWORD,
  //SET_LOGGED,
  SET_MESSAGE
} from './mutationTypes'

export default {
  handlerChangeUsername({ commit }, username) {
    // TODO - zapis na serwer
    commit(SET_USERNAME, username)
    commit(SET_MESSAGE, '')
  },
  handlerChangePassword({ commit }, password) {
    // TODO - - zapis na serwer
    commit(SET_PASSWORD, password)
    commit(SET_MESSAGE, '')
  },
  /*
  setUser({ commit }, user) {
    commit(SET_USER, user)
  },
  */
  setMessage({ commit }, message) {
    commit(SET_MESSAGE, message)
  }
}
