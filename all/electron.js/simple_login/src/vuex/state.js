export default {
  user: {
    id: null,
    username: 'test',
    password: 'test',
    logged: false,
    name: null,
    surname: null,
    email: null,
    for_authorization: null
  },
  message: ''
}
