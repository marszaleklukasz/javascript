import {
  SET_USERNAME,
  SET_PASSWORD,
  //SET_LOGGED,
  SET_MESSAGE,
  SET_USER
} from './mutationTypes'

export default {
  [SET_USERNAME](state, username) {
    state.user.username = username
  },
  [SET_PASSWORD](state, password) {
    state.user.password = password
  },
  [SET_USER](state, user) {
    state.user = user
  },
  /*
  [SET_LOGGED](state, logged) {
    state.user.logged = logged
  },
  */
  [SET_MESSAGE](state, message) {
    state.message = message
  }
}
