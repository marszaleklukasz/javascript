export default {
  getMessage(state) {
    return state.message
  },

  getLogged(state) {
    return state.user.logged
  },

  getUserId(state) {
    return state.user.id
  },
  getUsername(state) {
    return state.user.username
  },
  getPassword(state) {
    return state.user.password
  }
}
