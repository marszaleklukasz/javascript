import Vue from 'nativescript-vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    items: [
      { 
        name: 'Produkt Przykładowy Żakiet K013 - Odcienie czerwieni', 
        price: '89,05',
        urlShop: 'http://lukasz.sky-shop.pl/Produkt-Przykladowy-Zakiet-K013-Odcienie-czerwieni-p50',
        imageUrl: 'http://lukasz.sky-shop.pl/images/lukasz/0-1000/Zakiet_K013_Odcienie_czerwieni_%5B89%5D_80.jpg',
        imageUrlDetail: 'http://lukasz.sky-shop.pl/images/lukasz/0-1000/Zakiet_K013_Odcienie_czerwieni_%5B89%5D_300.jpg',
        description: 'Sukienka zaprojektowana przez najlepszych specjalistów z branży mody. Każda kobieta uwielba taki krój sukienkek. Jest on stylowy i uniwersalny, nadaje się na letnie słoneczne dni oraz na deszczowe jesienie. Warto zwrócić uwagę takze na krój który jest niebanalny i dopasowuje się idealnie do sylwetki kazdej kobiety',
        sizes: 'L/M/S',
        availability: 'Duża dostępność',
        shippingWithin: '24h',
        shippingPrice: '12,00 zł',
        weight: '0.35 kg'
      }, {
        name: 'Produkt Przykładowy Bluzka M110 - róż', 
        price: '67,00',
        urlShop: 'http://lukasz.sky-shop.pl/Produkt-Przykladowy-Bluzka-M110-roz-p22',
        imageUrl: 'http://lukasz.sky-shop.pl/images/lukasz/0-1000/Bluzka_M110_roz_%5B38%5D_80.jpg',
        imageUrlDetail: 'http://lukasz.sky-shop.pl/images/lukasz/0-1000/Bluzka_M110_roz_%5B38%5D_300.jpg',
        description: 'Sukienka zaprojektowana przez najlepszych specjalistów z branży mody. Każda kobieta uwielba taki krój sukienkek. Jest on stylowy i uniwersalny, nadaje się na letnie słoneczne dni oraz na deszczowe jesienie. Warto zwrócić uwagę takze na krój który jest niebanalny i dopasowuje się idealnie do sylwetki kazdej kobiety',
        sizes: 'L/M/S',
        availability: 'Mała dostępność',
        shippingWithin: '24h',
        shippingPrice: '12,00 zł',
        weight: '0.15 kg'        
      }, {
        name: 'Produkt Przykładowy Bluza z Kwiatowym Nadrukiem w Stylu Vintage', 
        price: '66,99',
        urlShop: 'http://lukasz.sky-shop.pl/Produkt-Przykladowy-Bluza-z-Kwiatowym-Nadrukiem-w-Stylu-Vintage-p6',
        imageUrl: 'http://lukasz.sky-shop.pl/images/lukasz/0-1000/Bluza_z_Kwiatowym_Nadrukiem_w_St_%5B12%5D_80.jpg',
        imageUrlDetail: 'http://lukasz.sky-shop.pl/images/lukasz/0-1000/Bluza_z_Kwiatowym_Nadrukiem_w_St_%5B12%5D_300.jpg',
        description: 'Sukienka zaprojektowana przez najlepszych specjalistów z branży mody. Każda kobieta uwielba taki krój sukienkek. Jest on stylowy i uniwersalny, nadaje się na letnie słoneczne dni oraz na deszczowe jesienie. Warto zwrócić uwagę takze na krój który jest niebanalny i dopasowuje się idealnie do sylwetki kazdej kobiety',
        sizes: 'L/M/S',
        availability: 'Duża dostępność',
        shippingWithin: '24h',
        shippingPrice: '16,00 zł',
        weight: '0.85 kg'        
      }, { 
        name: 'Produkt Przykładowy Bluzka K230 - Odcienie szarości', 
        price: '36,75',
        urlShop: 'http://lukasz.sky-shop.pl/Produkt-Przykladowy-Bluzka-K230-Odcienie-szarosci-p18',
        imageUrl: 'http://lukasz.sky-shop.pl/images/lukasz/0-1000/Bluzka_K230_Odcienie_szarosci_%5B26%5D_80.jpg',
        imageUrlDetail: 'http://lukasz.sky-shop.pl/images/lukasz/0-1000/Bluzka_K230_Odcienie_szarosci_%5B26%5D_300.jpg',
        description: 'Sukienka zaprojektowana przez najlepszych specjalistów z branży mody. Każda kobieta uwielba taki krój sukienkek. Jest on stylowy i uniwersalny, nadaje się na letnie słoneczne dni oraz na deszczowe jesienie. Warto zwrócić uwagę takze na krój który jest niebanalny i dopasowuje się idealnie do sylwetki kazdej kobiety',
        sizes: 'L/M/S',
        availability: 'Mała dostępność',
        shippingWithin: '24h',
        shippingPrice: '17,00 zł',
        weight: '0.45 kg'        
      }, { 
        name: 'Produkt Przykładowy Letni T-shirt z AZTECKIM RÓŻOWO-ŻÓŁTY Nadrukiem', 
        price: '27,99',
        urlShop: 'http://lukasz.sky-shop.pl/Produkt-Przykladowy-Letni-T-shirt-z-AZTECKIM-ROZOWO-ZOLTY-Nadrukiem-p38',
        imageUrl: 'http://lukasz.sky-shop.pl/images/lukasz/0-1000/Letni_Tshirt_z_AZTECKIM_ROZOWOZO_%5B72%5D_80.jpg',
        imageUrlDetail: 'http://lukasz.sky-shop.pl/images/lukasz/0-1000/Letni_Tshirt_z_AZTECKIM_ROZOWOZO_%5B72%5D_300.jpg',
        description: 'Sukienka zaprojektowana przez najlepszych specjalistów z branży mody. Każda kobieta uwielba taki krój sukienkek. Jest on stylowy i uniwersalny, nadaje się na letnie słoneczne dni oraz na deszczowe jesienie. Warto zwrócić uwagę takze na krój który jest niebanalny i dopasowuje się idealnie do sylwetki kazdej kobiety',
        sizes: 'L/M/S',
        availability: 'Duża dostępność',
        shippingWithin: '24h',
        shippingPrice: '12,00 zł',
        weight: '0.75 kg'        
      }, {
        name: 'Produkt Przykładowy Bluzka M325 - mięta', 
        price: '153,00',
        urlShop: 'http://lukasz.sky-shop.pl/Produkt-Przykladowy-Bluzka-M325-mieta-p26',
        imageUrl: 'http://lukasz.sky-shop.pl/images/lukasz/0-1000/Bluzka_M325_mieta_%5B51%5D_80.jpg',
        imageUrlDetail: 'http://lukasz.sky-shop.pl/images/lukasz/0-1000/Bluzka_M325_mieta_%5B51%5D_300.jpg',
        description: 'Sukienka zaprojektowana przez najlepszych specjalistów z branży mody. Każda kobieta uwielba taki krój sukienkek. Jest on stylowy i uniwersalny, nadaje się na letnie słoneczne dni oraz na deszczowe jesienie. Warto zwrócić uwagę takze na krój który jest niebanalny i dopasowuje się idealnie do sylwetki kazdej kobiety',
        sizes: 'L/M/S',
        availability: 'Duża dostępność',
        shippingWithin: '24h',
        shippingPrice: '12,00 zł',
        weight: '0.25 kg'        
      }, { 
        name: 'Produkt Przykładowy Bluzka M341 - fuksja', 
        price: '188,00',
        urlShop: 'http://lukasz.sky-shop.pl/Produkt-Przykladowy-Bluzka-M341-fuksja-p27',
        imageUrl: 'http://lukasz.sky-shop.pl/images/lukasz/0-1000/Bluzka_M341_fuksja_%5B55%5D_80.jpg', 
        imageUrlDetail: 'http://lukasz.sky-shop.pl/images/lukasz/0-1000/Bluzka_M341_fuksja_%5B55%5D_300.jpg', 
        description: 'Sukienka zaprojektowana przez najlepszych specjalistów z branży mody. Każda kobieta uwielba taki krój sukienkek. Jest on stylowy i uniwersalny, nadaje się na letnie słoneczne dni oraz na deszczowe jesienie. Warto zwrócić uwagę takze na krój który jest niebanalny i dopasowuje się idealnie do sylwetki kazdej kobiety',
        sizes: 'L/M/S',
        availability: 'Duża dostępność',
        shippingWithin: '24h',
        shippingPrice: '11,00 zł',
        weight: '0.95 kg'        
      }, { 
        name: 'Produkt Przykładowy Spodenki K159 - Odcienie błekitu', 
        price: '49,00',
        urlShop: 'http://lukasz.sky-shop.pl/Produkt-Przykladowy-Spodenki-K159-Odcienie-blekitu-p40',
        imageUrl: 'http://lukasz.sky-shop.pl/images/lukasz/0-1000/Spodenki_K159_Odcienie_blekitu_%5B79%5D_80.jpg',
        imageUrlDetail: 'http://lukasz.sky-shop.pl/images/lukasz/0-1000/Spodenki_K159_Odcienie_blekitu_%5B79%5D_300.jpg',
        description: 'Sukienka zaprojektowana przez najlepszych specjalistów z branży mody. Każda kobieta uwielba taki krój sukienkek. Jest on stylowy i uniwersalny, nadaje się na letnie słoneczne dni oraz na deszczowe jesienie. Warto zwrócić uwagę takze na krój który jest niebanalny i dopasowuje się idealnie do sylwetki kazdej kobiety',
        sizes: 'L/M/S',
        availability: 'Duża dostępność',
        shippingWithin: '24h',
        shippingPrice: '19,00 zł',
        weight: '0.75 kg'        
      }, { 
        name: 'Produkt Przykładowy Spodnie K044 Wyprzedaż !!! - Odcienie czarnego', 
        price: '29,00',
        urlShop: 'http://lukasz.sky-shop.pl/Produkt-Przykladowy-Spodnie-K044-Wyprzedaz-Odcienie-czarnego-p43',
        imageUrl: 'http://lukasz.sky-shop.pl/images/lukasz/0-1000/Spodnie_K044_Wyprzedaz_Odcienie__%5B82%5D_80.jpg',
        imageUrlDetail: 'http://lukasz.sky-shop.pl/images/lukasz/0-1000/Spodnie_K044_Wyprzedaz_Odcienie__%5B82%5D_300.jpg',
        description: 'Sukienka zaprojektowana przez najlepszych specjalistów z branży mody. Każda kobieta uwielba taki krój sukienkek. Jest on stylowy i uniwersalny, nadaje się na letnie słoneczne dni oraz na deszczowe jesienie. Warto zwrócić uwagę takze na krój który jest niebanalny i dopasowuje się idealnie do sylwetki kazdej kobiety',
        sizes: 'L/M/S',
        availability: 'Duża dostępność',
        shippingWithin: '24h',
        shippingPrice: '18,00 zł',
        weight: '0.45 kg'
      }            
    ]
  },
  mutations: {

  },
  actions: {

  },
  getters: {
    items: state => {
      return state.items;
    }
  }  
});
