import Vue from 'nativescript-vue'
import RadListView from 'nativescript-ui-listview/vue';
import App from './components/App'
import store from './store'

Vue.config.silent = (TNS_ENV === 'production')

Vue.use(RadListView);

new Vue({
  store,
  render: h => h('frame', [h(App)])
}).$start()
