const app = require('node-server-screenshot');

const url = 'https://www.mydevil.net/';
const fileName = 'screenshot.png';

try {
    app.fromURL(url, fileName, function() {
        console.log(`Zrzut ekranu strony ${url} do pliku ${fileName}`);
    });
} catch(e) {
    console.log('Error:', e.stack);
}
