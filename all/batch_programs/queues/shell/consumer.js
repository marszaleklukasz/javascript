const path = require('path');
require('../lib/app').initPathApp(path.dirname(__dirname));
const libUtilities = require(path.join(pathApp.libDir, 'utilities'));
const { TaskBroker } = require(path.join(pathApp.libDir, 'taskBroker'));

process.on('unhandledRejection', error => {
  console.log('Nieobsłużone odrzucenie obietnicy', error);
});

process.on('rejectionHandled', error => {
  console.log('Nieobsłużone - rejectionHandled');
});

const taskBroker = new TaskBroker('sample-queue');

(async () => {
  await taskBroker.getQueue(async (task) => {
    console.log(task);
  });
})();