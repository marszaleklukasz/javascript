const path = require('path');
require('../lib/app').initPathApp(path.dirname(__dirname));
const libUtilities = require(path.join(pathApp.libDir, 'utilities'));
const { TaskBroker } = require(path.join(pathApp.libDir, 'taskBroker'));

process.on('unhandledRejection', error => {
  console.log('Nieobsłużone odrzucenie obietnicy', error);
});

process.on('rejectionHandled', error => {
  console.log('Nieobsłużone - rejectionHandled');
});

const taskBroker = new TaskBroker('sample-queue');

(async () => {
  for (let i = 1; i <= 40000; i++) {
    let msg = {
      message: `Zadanie ${i}!`
    };

    await taskBroker.addOneTask(msg);
  }

  await taskBroker.queue.close();
  process.exit(0);
})();