const path = require('path');
const is = require('is');
const { rabbitmq } = require(path.join(pathApp.configDir, 'app.json'));
const WorkQueue = require('wrappitmq').WorkQueue;
const libUtilities = require(path.join(pathApp.libDir, 'utilities'));

let name = 'default';

if (process.env.NODE_ENV) {
  name = process.env.NODE_ENV;
}

module.exports.TaskBroker = class TaskBroker {
  constructor(queueName) {
    if (is.defined(TaskBroker.instance) && is.defined(TaskBroker.instance[queueName])) {
      return TaskBroker.instance[queueName];
    }

    this.queueName = queueName;

    this.queue = new WorkQueue({
      prefetch: 1,
      queue: queueName,
      url: `amqp://${rabbitmq[name].user}:${rabbitmq[name].pass}@${rabbitmq[name].host}:${rabbitmq[name].port}`
    });

    this.queue.on('error', (err) => {
      console.error(err);
    });

    this.queue.on('close', (err) => {
      console.log('Close queue');
    });

    if (!is.defined(TaskBroker.instance)) {
      TaskBroker.instance = {};
    }

    TaskBroker.instance[queueName] = this;
  }

  async connect() {
    await this.queue.connect();
  }

  async getQueue(cb) {
    if (!this.queue.connection) {
      await this.connect();
    }

    await this.queue.consume(cb);
  }

  async addOneTask(msg) {
    if (!this.queue.connection) {
      await this.connect();
    }

    await this.queue.enqueue(msg);

    console.log(`>>>>>Do kolejki dodano zadanie: "${libUtilities.serialize(msg)}"<<<<<`);
  }
};
