const replacer = (k, v) => {
  let result = v;

  if (typeof v === 'function') {
    result = v.toString();
  }

  return result;
};

const reviver = (k, v) => {
  let result = v;

  if (typeof v === 'string' && v.includes('function ()', 0)) {
    result = eval(`(${v})`);
  }

  return result;
};

exports.serialize = (v) => {
  return JSON.stringify(v, replacer);
};

exports.deserialize = (v) => {
  return JSON.parse(v, reviver);
};