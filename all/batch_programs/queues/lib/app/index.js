const path = require('path');

module.exports = {
  initPathApp: (appDir) => {
    global.pathApp = {
      appDir: appDir,
      libDir:  path.join(appDir, 'lib'),
      configDir: path.join(appDir, 'config')
    }
  }
};